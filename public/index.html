<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Illuminating the I/O Optimization Path of Scientific Applications</title>

		<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono:400,400i|IBM+Plex+Sans+Condensed:400,400i|IBM+Plex+Sans:100,100i,400,400i,700,700i|IBM+Plex+Serif:400,400i" rel="stylesheet">
        <link rel="stylesheet" href="css/default.css"/>

        <!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-43946099-4"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-43946099-4');
		</script>
    </head>
    <body>

    	<p>
			<b class="label-black">COMPANION WEBSITE</b>
		</p>

		<h1>Illuminating the I/O Optimization Path of Scientific Applications</h1>

		<p>
			<em>Hammad Ather<sup>1, 2</sup>, Jean Luca Bez<sup>1</sup>, Boyana Norris<sup>2</sup>, Suren Byna<sup>1, 3</sup></em>
		</p>

		<p class="institutions">
			<sup>1</sup> Lawrence Berkeley National Laboratory, <sup>2</sup> University of Oregon, <sup>3</sup> The Ohio State University
		</p>

		<h2>Contents</h2>

		<p class="information">
			This repository contains the interactive visualizations from our ISC 2023 paper:
		</p>

		<p class="url">
			<a href="https://jeanbez.gitlab.io/isc23">https://jeanbez.gitlab.io/isc23</a>
		</p>

		<p>
			<img src="images/drishti.png" />
		</p>

		<p class="information">
			Drishti is a tool to generate interactive data visualizations of Darshan I/O traces collected from HPC applications. Its goal is to provide an easy and interactive way for researchers and developers to explore their application's I/O behavior and detect possible I/O bottlenecks that are impacting performance. You can check it out at:
		</p>

		<p class="url">
			<a href="https://github.com/hpc-io/drishti">https://github.com/hpc-io/drishti</a>
		</p>

		<p class="code">
			git clone https://github.com/hpc-io/drishti
		</p>

		<p class="information">
			Drishti will generate a explore.html file with an interactive plot that you can open in any browser to explore. A sample .darshan file is provided inside <span>samples</span>.
		</p>

		<h2>Interactive Plots</h2>
		
		<p class="information">
			Note: Some of the interactive plots might take some time to load due to large size.
	   </p>
	   
		<p class="information">
 			You can explore the following interactive plots:
		</p>

		<ul>
			<li>
				<a href="interactive/cori/openpmd-original/explore-transfer.html"><b class="label-blue">BONUS</b></a> OpenPMD Cori: best baseline write execution (transfer size)
			</li>
			<li>
				<a href="interactive/cori/openpmd-original/explore.html"><b class="label-gray">FIGURE 8</b></a> OpenPMD Cori: best baseline write execution (operation)
			</li>
			<li>
				<a href="interactive/cori/openpmd-optimized/explore-transfer.html"><b class="label-blue">BONUS</b></a> OpenPMD Cori: best optimized write execution (transfer size)
			</li>
			<li>
				<a href="interactive/cori/openpmd-optimized/explore.html"><b class="label-blue">BONUS</b></a> OpenPMD Cori: best optimized write execution (operation)
			</li>
			<li>
				<a href="interactive/cori/write-original/explore.html"><b class="label-blue">BONUS</b></a> Write Cori: best baseline execution (operation)
			</li>
			<li>
				<a href="interactive/cori/write-optimized/explore.html"><b class="label-blue">BONUS</b></a> Write Cori: best optimized execution (operation)
			</li>
			<li>
				<a href="interactive/cori/e2e-original/explore.html"><b class="label-blue">BONUS</b></a> e2e Cori: best baseline execution (operation)
			</li>
			<li>
				<a href="interactive/cori/e2e-optimized/explore.html"><b class="label-blue">BONUS</b></a> e2e Cori: best optimized execution (operation)
			</li>
			<li>
				<a href="interactive/cori/amrex-original/explore.html"><b class="label-red">TABLE 3</b></a> Amrex Cori: best baseline execution (operation)
			</li>
			<li>
				<a href="interactive/cori/amrex-optimized/explore.html"><b class="label-red">TABLE 3</b></a> Amrex Cori: best optimized execution (operation)
			</li>
		</ul>

		<p class="information">
			Complementary plots that depict the transfer size and spatiality can be found in the repository under <span class="inline-code">additional-plots.tar.gz</span>. You can clone the repository and open the <span class="inline-code">.html</span> file in your preferred browser.
		</p>

		<h2>Additional Experiments</h2>

		<p class="information">
			In this section, we present additional snapshots from the Drishti tool used in the paper.
		</p>

		<h3>E2E Benchmark in Cori</h3>

		<p class="information">
			Figure 1 shows the behavior of E2E at POSIX and MPI-IO level in Cori. In the plot we can see a lot of the time is taken by rank 0 sequentially writing fill values to all of the defined variables (10 in this workload). Figure 1 shows the runtime for this optimized version, after explicitly disabling the data filling behavior.
		</p>

		<img src="images/005-paper-1-summit-baseline.png" />
		<p class="information">Figure 1: Best exeuction of the baseline E2E benchmark in Cori. It is possible to see the long time spent by rank $0$ sequentially writing fill values to file.</p>

		<img src="images/007-paper-1-summit-no-fill.png" />
		<p class="information">Figure 2: Best exeuction of E2E after tuned for cori.</p>

		<div class="copyright">
			<p>
				<strong>LICENSE</strong>
			</p>
			<p>
				DXT Explorer Copyright (c) 2022, The Regents of the University of California, through Lawrence Berkeley National Laboratory (subject to receipt of any required approvals from the U.S. Dept. of Energy). All rights reserved.
			</p>
			<p>
				If you have questions about your rights to use or distribute this software, please contact Berkeley Lab's Intellectual Property Office at IPO@lbl.gov.
			</p>
			<p>
				<strong>NOTICE</strong>. This Software was developed under funding from the U.S. Department of Energy and the U.S. Government consequently retains certain rights.  As such, the U.S. Government has been granted for itself and others acting on its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the Software to reproduce, distribute copies to the public, prepare derivative works, and perform publicly and display publicly, and to permit others to do so.
			</p>
		</p>

    </body>
</html>

