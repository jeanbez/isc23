# Illuminating the I/O Optimization Path of Scientific Applications

Hammad Ather (1,2), Jean Luca Bez (1), Boyana Norris (2), Suren Byna (1,3)

(1) Lawrence Berkeley National Laboratory
(2) University of Oregon
(3) The Ohio State University

This repository contains the logs and analysis of our ISC 2023 paper. For the companion website please refer to:

https://jeanbez.gitlab.io/isc23

## Drishti

Drishti is a tool to generate interactive data visualizations of Darshan I/O traces collected from HPC applications. Its goal is to provide an easy and interactive way for researchers and developers to explore their application's I/O behavior and detect possible I/O bottlenecks that are impacting performance. You can check it out at:

```
git clone https://gitlab.com/hpc-io/drishti
```

## Contents

In the `additional-plots.tar.gz` file you can find all the interactive plots versions from our paper. These HTML files can be opened with your preferred browser to explore the operation, transfer size, and spatiality of DXT traces.